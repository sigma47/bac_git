// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class TreeAnimatorPlugin : ModuleRules
	{
        public TreeAnimatorPlugin(TargetInfo Target)
		{
			PublicIncludePaths.AddRange(new string[] {"TreeAnimatorPlugin/Public",});
            PrivateIncludePaths.AddRange(new string[] {"TreeAnimatorPlugin/Private",});

			PublicDependencyModuleNames.AddRange(new string[] {"Core",});
		}
	}
}