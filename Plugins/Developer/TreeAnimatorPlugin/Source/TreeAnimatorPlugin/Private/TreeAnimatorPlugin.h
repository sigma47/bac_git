#pragma once

#include "ModuleManager.h"

class TreeAnimatorPlugin : public IModuleInterface
{
public:
    /** IModuleInterface implementation */
    void StartupModule();
    void ShutdownModule();
};