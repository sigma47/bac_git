// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeBone.h"
#include "TreeNode.h"
#include "TreeBranch.h"
#include "TreeLeaf.h"

// Sets default values
ATreeBone::ATreeBone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	auto transformComp = CreateDefaultSubobject<USceneComponent>(FName("USceneComponent"), true);
	AddComponent(FName("USceneComponent"), true, FTransform(FVector()), transformComp);
	SetRootComponent(transformComp);

	lenCUpdThisTick = false;;
	lengthCache = 0;
	ParentBranch = nullptr;

	dirCUpdThisTick = false;
	dirCache = FVector::ZeroVector;

	currPhi = 0;
	currGamma = 0;
}

// Called when the game starts or when spawned
void ATreeBone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreeBone::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

float ATreeBone::Length()
{
	if (!lenCUpdThisTick)
	{
		lengthCache = FMath::Sqrt(LengthSqr());
		lenCUpdThisTick = true;
	}
	return lengthCache;
}

float ATreeBone::LengthSqr()
{
	return FVector::DistSquared(EndNode->GetActorLocation(), StartNode->GetActorLocation());
}

FVector ATreeBone::Direction()
{
	if (!dirCUpdThisTick)
	{
		dirCache = (EndNode->GetActorLocation() - StartNode->GetActorLocation()).GetSafeNormal();
		dirCUpdThisTick = true;
	}
	return dirCache;
}

void ATreeBone::AttachToBranch(ATreeBranch* parent, ATreeNode* start)
{
	ParentBranch = parent;
	parent->Bones.Add(this);

	if (start != StartNode)
	{
		ATreeNode* tmp = StartNode;
		StartNode = start;
		EndNode = tmp;
	}
	
	FVector diff = GetActorLocation();

	SetActorLocation(StartNode->GetActorLocation());
	auto rootingB = parent->GetRootingBone(this);
	if (rootingB)
		AttachRootComponentToActor(rootingB, NAME_None, EAttachLocation::Type::KeepWorldPosition);
	EndNode->AttachRootComponentToActor(this, NAME_None, EAttachLocation::Type::KeepWorldPosition);

	diff = GetActorLocation() - diff;
	for (ATreeLeaf* l : Leaves)
	{
		l->SetActorLocation(l->GetActorLocation() - diff);
		l->BranchNode->SetActorLocation(l->BranchNode->GetActorLocation() - diff);
		for (FVector vert : l->LeafVertices)
			vert -= diff;
	}
}

void ATreeBone::MoveBoneWOChildren(FVector newPosition)
{
	FVector diff = GetActorLocation();

	SetActorLocation(newPosition);
	diff = GetActorLocation() - diff;

	for (ATreeLeaf* l : Leaves)
	{
		l->SetActorLocation(l->GetActorLocation() - diff);
		l->BranchNode->SetActorLocation(l->BranchNode->GetActorLocation() - diff);
		for (FVector vert : l->LeafVertices)
			vert -= diff;
	}
}

bool ATreeBone::IsSameBranch(ATreeBone* b)
{
	if (ParentBranch && b->ParentBranch)
		return ParentBranch == b->ParentBranch;

	//if angle between branches is below 25� -> same generation
	FVector aDir = EndNode->GetActorLocation() - StartNode->GetActorLocation();
	FVector bDir = b->EndNode->GetActorLocation() - b->StartNode->GetActorLocation();

	float angle = (FVector::DotProduct(aDir, bDir)) / (aDir.Size() * bDir.Size());
	angle = FMath::RadiansToDegrees(FMath::Acos(angle));
	if (angle >= 180)
		angle -= 180;
	if (angle >= 0 && angle <= 25)
		return true;
	else
		return false;
}

void ATreeBone::TickReset()
{
	lenCUpdThisTick = false;
	dirCUpdThisTick = false;
}
