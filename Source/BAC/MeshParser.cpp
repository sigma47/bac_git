// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "MeshParser.h"
#include "TreeSkeleton.h"
#include "TreeBone.h"
#include "TreeNode.h"
#include "Editor/EditorEngine.h"
#include "TreeLeaf.h"
#include "TreeFull.h"
//#include "TreeAnimatorPluginPCH.h"
#include "BACBlueprints.h"

DEFINE_LOG_CATEGORY(MeshParser);


AMeshParser::AMeshParser(const class FObjectInitializer& ObjectInitializer)
{
}

ATreeSkeleton* AMeshParser::ParseFile(FString fileDir, UObject* worldCtxObject, float scale)
{
	/*
	if (!GEngine)
	{
	UE_LOG(MeshParser, Log, L"GEngine not set.");
	}

	if (!GEngine->GetWorld())
	{
	UE_LOG(MeshParser, Log, L"World not set.");
	}

	if (!GWorld)
	{
	UE_LOG(MeshParser, Log, L"GWorld not set.");
	}

	if (!GEngine->IsEditor())
	{
	UE_LOG(MeshParser, Log, L"Is not an Editor.");
	}

	//auto edeng = Cast<UEditorEngine>(GEngine);
	//auto wctx = edeng->GetEditorWorldContext(true);

	/*if (!wctx.World())
	{
		UE_LOG(MeshParser, Log, L"Got no world context.");
	}

	//auto lvls = wctx.World()->GetLevels();
	if (lvls.Num() < 1)
	{
		UE_LOG(MeshParser, Log, L"Got no lvls.");
	}*/

	//ATreeSkeleton* skeleton = nullptr;//(ATreeSkeleton*)edeng->AddActor(lvls[0], ATreeSkeleton::StaticClass(), FTransform::Identity);
	//ATreeSkeleton* skeleton = (ATreeSkeleton*)worldCtx->SpawnActor(ATreeSkeleton::StaticClass());

	const FString skelDir = "_Skel.obj";
	const FString fullDir = "_Full+Leaf.obj";

	ATreeSkeleton* skeleton = (ATreeSkeleton*)worldCtxObject->GetWorld()->SpawnActor<ATreeSkeleton>(ATreeSkeleton::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	skeleton->Scale = scale;
	
	ATreeFull* tree = (ATreeFull*)worldCtxObject->GetWorld()->SpawnActor<ATreeFull>(ATreeFull::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);

	ParseSkeleton(fileDir + skelDir, *skeleton, worldCtxObject, scale);

	ParseFullMesh(fileDir + fullDir, *skeleton, *tree, worldCtxObject, scale);

	//skeleton->OrderBones();

	int32 a = sizeof(ATreeSkeleton);
	int32 b = sizeof(ATreeBone);
	//int32 c = sizeof(ATreeBranch);
	int32 d = sizeof(ATreeNode);
	int32 e = sizeof(ATreeLeaf);

	FString s = " class sizes: a:";
	s += FString::SanitizeFloat(a);
	s += " b: ";
	s += FString::SanitizeFloat(b);
	s += " d: ";
	s += FString::SanitizeFloat(d);
	s += " e: ";
	s += FString::SanitizeFloat(e);
	UE_LOG(LogActor, Warning, TEXT("%s"), *s);


	return skeleton;
}

void AMeshParser::ParseSkeleton(FString fileDir, ATreeSkeleton& skeleton, UObject* worldCtxObject, float scale)
{
	TArray<FString> strings;
	TArray<ATreeNode*> nodes;

	if (!FFileHelper::LoadANSITextFileToStrings(*fileDir, NULL, strings))
	{
		UE_LOG(MeshParser, Log, L"Could not find file.");
		UE_LOG(MeshParser, Log, TEXT("%s"), *fileDir);
		return;
	}

	for (FString s : strings)
	{
		int32 commandPos = -1;
		if (!s.FindChar(' ', commandPos))
		{
			FString err = "Invalid command format: ";
			err += s;
			err += " .";
			UE_LOG(MeshParser, Log, TEXT("%s"), *err);
			return;
		}
		FString command = s.Left(commandPos);
		s = s.Mid(commandPos + 1);
		FString args = s;

		if (command == "v")
		{
			ATreeNode* node = (ATreeNode*)worldCtxObject->GetWorld()->SpawnActor<ATreeNode>(ATreeNode::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
			if (AMeshParser::CreateNode(args, node, scale))
				nodes.Add(node);
		}
		else if (command == "l")
		{
			ATreeBone* bone = (ATreeBone*)worldCtxObject->GetWorld()->SpawnActor<ATreeBone>(ATreeBone::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
			bone->AttachRootComponentToActor(&skeleton);
			bone->SetOwner(&skeleton);
			if (AMeshParser::CreateBone(args, *bone, nodes, worldCtxObject))
			{
				skeleton.Bones.Add(bone);
				bone->StartNode->AttachRootComponentToActor(&skeleton);
				bone->EndNode->AttachRootComponentToActor(&skeleton);
			}
		}
	}
}

void AMeshParser::ParseFullMesh(FString fileDir, ATreeSkeleton& skeleton, ATreeFull& tree, UObject* worldCtxObject, float scale)
{
	TArray<FString> strings;
	TArray<FVector> vertices;
	bool IsLeafMode = false;

	if (!FFileHelper::LoadANSITextFileToStrings(*fileDir, NULL, strings))
	{
		UE_LOG(MeshParser, Log, L"Could not find file.");
		UE_LOG(MeshParser, Log, TEXT("%s"), *fileDir);
		return;
	}

	for (FString s : strings)
	{
		int32 commandPos = -1;
		if (!s.FindChar(' ', commandPos))
		{
			FString err = "Invalid command format: ";
			err += s;
			err += " .";
			UE_LOG(MeshParser, Log, TEXT("%s"), *err);
			return;
		}
		FString command = s.Left(commandPos);
		s = s.Mid(commandPos + 1);
		FString args = s;

		if (command == "o")
		{
			if (args == "tree")
				IsLeafMode = false;
			else if (args == "leaves")
				IsLeafMode = true;
			else
			{
				UE_LOG(MeshParser, Log, L"Unknown mesh-model.");
				IsLeafMode = false;
			}
		}
		else if (command == "v")
		{
			vertices.Add(ParseVertex(args, scale));
		}
		else if (command == "f" && IsLeafMode)
		{
			ATreeLeaf* leaf = (ATreeLeaf*)worldCtxObject->GetWorld()->SpawnActor<ATreeLeaf>(ATreeLeaf::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
			//leaf->AttachRootComponentToActor(&skeleton);
			leaf->SetOwner(&skeleton);
			if (AMeshParser::CreateLeaf(args, *leaf, vertices, worldCtxObject))
			{
				if (!FindLeafBranch(leaf, &skeleton, scale, worldCtxObject))
					skeleton.LostLeaves.Add(leaf);
			}
		}
	}
}

bool AMeshParser::CreateNode(FString args, ATreeNode* node, float scale)
{
	node->SetActorLocation(ParseVertex(args, scale));

	/*
	FString s = " new vertex: x:";
	s += FString::SanitizeFloat(x);
	s += " y: ";
	s += FString::SanitizeFloat(y);
	s += " z: ";
	s += FString::SanitizeFloat(z);
	UE_LOG(LogActor, Warning, TEXT("%s"), *s);
	*/

	return true;
}

bool AMeshParser::CreateBone(FString args, ATreeBone& bone, TArray<ATreeNode*>& nodes, UObject* worldCtxObject)
{
	int32 a, b;
	int32 pos = -1;
	if (!args.FindChar(' ', pos))
	{
		UE_LOG(MeshParser, Log, L"Invalid line(l) format.");
		return false;
	}
	a = FCString::Atoi(*args.Left(pos));
	args = args.Mid(pos + 1);
	b = FCString::Atoi(*args);
	
	/*
	FString s = " new bone: a:";
	s += FString::SanitizeFloat(a);
	s += " b: ";
	s += FString::SanitizeFloat(b);
	UE_LOG(LogActor, Warning, TEXT("%s"), *s);
	*/

	ATreeNode* Start = nodes[a - 1];
	ATreeNode* End = nodes[b - 1];

	FVector aLoc = Start->GetActorLocation();
	FVector bLoc = End->GetActorLocation();

	bone.StartNode = Start;
	bone.EndNode = End;
	bone.SetActorLocation(aLoc + 0.5F * (bLoc - aLoc));

	Start->Bones.Add(&bone);
	End->Bones.Add(&bone);
	//Start->AttachRootComponentToActor(&bone);
	//End->AttachRootComponentToActor(&bone);

	return true;
}

bool AMeshParser::CreateLeaf(FString args, ATreeLeaf& leaf, TArray<FVector>& vertices, UObject* worldCtxObject)
{
	TArray<int32> nodeIndcs = AMeshParser::ParseFace(args);
	TArray<FVector> verts;

	for (int32 idx : nodeIndcs)
	{
		verts.Add(vertices[idx - 1]);
	}
	leaf.SetVertices(verts);

	return true;
}

FVector AMeshParser::ParseVertex(FString args, float scale)
{
	FVector result = FVector::ZeroVector;
	float x, y, z;
	int32 pos = -1;

	if (!args.FindChar(' ', pos))
	{
		UE_LOG(MeshParser, Log, L"Invalid vertice(v) format.");
		return result;
	}
	x = FCString::Atof(*args.Left(pos));

	args = args.Mid(pos + 1);
	if (!args.FindChar(' ', pos))
	{
		UE_LOG(MeshParser, Log, L"Invalid vertice(v) format.");
		return result;
	}
	z = FCString::Atof(*args.Left(pos));

	args = args.Mid(pos + 1);
	y = FCString::Atof(*args);

	result = FVector(x, y, z) * scale;

	return result;
}

TArray<int32> AMeshParser::ParseFace(FString args)
{
	TArray<int32> finalArgs;
	FString subArg;
	int32 pos1, pos2;
	for (int i = 0; i < 4; i++)
	{
		if (i < 3)
		{
			if (!args.FindChar(' ', pos1))
			{
				UE_LOG(MeshParser, Log, L"Invalid face(f) format.");
				return finalArgs;
			}
			subArg = args.Left(pos1);
			args = args.Mid(pos1 + 1);
		}
		else
			subArg = args;
		if (!subArg.FindChar('/', pos2))
		{
			UE_LOG(MeshParser, Log, L"Invalid face(f) format.");
			return finalArgs;
		}
		subArg = subArg.Left(pos2);
		finalArgs.Add(FCString::Atoi(*subArg));
	}
	return finalArgs;
}

bool AMeshParser::FindLeafBranch(ATreeLeaf* leaf, ATreeSkeleton* skeleton, float scale, UObject* worldCtxObject)
{
	// find bone to attach leaf to
	// set branch node in leaf
	// evtl. set parent of leaf to branch

	FVector leafCenter = leaf->GetActorLocation();
	float totalMinDst = 10000000;
	ATreeBone* bMinDst = nullptr;
	FVector projMinDst = FVector::ZeroVector;

	for (ATreeBone* bone : skeleton->Bones)
	{
		FVector startPos = bone->StartNode->GetActorLocation();
		FVector endPos = bone->EndNode->GetActorLocation();
		FVector sToCentDir = leafCenter - startPos;

		float minDst = UBACBlueprints::PointToVecDst(startPos, endPos, leafCenter);
		if (totalMinDst > minDst)
		{
			totalMinDst = minDst;
			bMinDst = bone;
			projMinDst = endPos;
		}
	}

	if (bMinDst && totalMinDst < 25.0)
	{
		ATreeNode* node = (ATreeNode*)worldCtxObject->GetWorld()->SpawnActor<ATreeNode>(ATreeNode::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
		node->SetActorLocation(projMinDst - bMinDst->GetActorLocation());
		node->AttachRootComponentToActor(bMinDst);
		node->Bones.Add(bMinDst);
		//node->SetActorLocation(projMinDst);
		//node->AttachRootComponentToActor(skeleton);
		leaf->BranchNode = node;
		leaf->AttachRootComponentToActor(bMinDst);
		leaf->SetActorLocation(leaf->GetActorLocation() - bMinDst->GetActorLocation());
		node->Leaves.Add(leaf);
		bMinDst->Leaves.Add(leaf);
		return true;
	}

	return false;
}