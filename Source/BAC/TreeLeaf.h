// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TreeLeaf.generated.h"

class ATreeNode;

UCLASS()
class BAC_API ATreeLeaf : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<FVector> LeafVertices;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<FVector> LeafVertsRelativeOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	ATreeNode* BranchNode;

	// Sets default values for this actor's properties
	ATreeLeaf();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void DrawDebug();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void SetVertices(TArray<FVector> verts);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void UpdateVerticesPosition();

};
