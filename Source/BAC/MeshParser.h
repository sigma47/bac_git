// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "MeshParser.generated.h"

class ATreeSkeleton;
class ATreeBone;
class ATreeNode;
class ATreeLeaf;
class ATreeFull;

DECLARE_LOG_CATEGORY_EXTERN(MeshParser, Log, All);

/**
 * 
 */
UCLASS(Blueprintable)
class BAC_API AMeshParser : public AActor
{
	GENERATED_BODY()

private:

	static bool CreateNode(FString args, ATreeNode* node, float scale);
	static bool CreateBone(FString args, ATreeBone& bone, TArray<ATreeNode*>& nodes, UObject* worldCtxObject);
	static bool CreateLeaf(FString args, ATreeLeaf& leaf, TArray<FVector>& vertices, UObject* worldCtxObject);

	static void ParseSkeleton(FString fileDir, ATreeSkeleton& skeleton, UObject* worldCtxObject, float scale);
	static void ParseFullMesh(FString fileDir, ATreeSkeleton& skeleton, ATreeFull& tree, UObject* worldCtxObject, float scale);

	static FVector ParseVertex(FString args, float scale);
	static bool FindLeafBranch(ATreeLeaf* leaf, ATreeSkeleton* skeleton, float scale, UObject* worldCtxObject);

	static TArray<int32> ParseFace(FString args);

public:

	AMeshParser(const class FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	static ATreeSkeleton* ParseFile(FString fileDir, UObject* worldCtxObject, float scale);


};
