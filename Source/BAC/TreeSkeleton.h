// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TreeSkeleton.generated.h"

class ATreeBone;
class ATreeNode;
class ATreeLeaf;
class ATreeBranch;

UCLASS()
class BAC_API ATreeSkeleton : public AActor
{
	GENERATED_BODY()
	
public:	

	const float IS_ON_BRANCH = 0.0000125;
	const float IS_OFF_BRANCH = 0.005;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	FVector NaturalFrequency;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	FVector DampeningRatio;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float TreeDensity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float UpdateStep;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float c0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float EImax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<ATreeBone*> Bones;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<ATreeBranch*> Branches;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float Scale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	bool DrawDebugInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	bool DrawBones;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	bool DrawNodes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	bool DrawLeaves;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float Scale0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float Ratio;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float RatioPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float BaseSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float Taper;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	float Scale0Trunk;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<float> LengthN;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TreeAnimator")
	TArray<ATreeLeaf*> LostLeaves;
	
	// Sets default values for this actor's properties
	ATreeSkeleton();
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// orders the bones direction to confirm with grwoth-direction, sets some member variables and packs them into branches
	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void OrderBones();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void OrderLeafNodes();

	//branchRatio: relative distance to total branch length at which node sits on branch. 0.0 = base, 1.0 = tip
	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float CalcNodeRadius(ATreeNode* node, float lengthOffset, float lengthMax);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float NewBranchRatio(ATreeNode* newRootNode, ATreeNode* rootStart, ATreeNode* rootEnd);

	//UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	//float GetParentNode(int32 generation, float branchRatio, ATreeNode* parentNode);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void GetFloatingBranches(TArray<ATreeBone*>& floatBones);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	bool IsOnLine(FVector p, FVector a, FVector b);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float GetLengthGenN(int32 generation, ATreeBranch* parent);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float CalcGamma(float Lv_, int32 n);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float CalcPhi(float ki, FVector si, FVector Fi, float x, int32 iters);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void UpdateHarmonicOscs(float deltaTime);

private:

	bool finishedSkeletonSetup;

	float currUpdateStep;
};
