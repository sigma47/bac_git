// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TreeNode.generated.h"

class ATreeBone;
class ATreeLeaf;
class ATreeBranch;

UCLASS()
class BAC_API ATreeNode : public AActor
{
	GENERATED_BODY()
	
public:	

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//FVector OrigPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ATreeBone*> Bones;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ATreeLeaf*> Leaves;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Lt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector L_t;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Ft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector QT;

	// Sets default values for this actor's properties
	ATreeNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeBone* GetAdjacentBoneWithParent(TArray<ATreeBone*> ignoreBranches);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	int32 GetLowestGeneration();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeBranch* GetParentBranch();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float GetRelativeBranchLength(float scale);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	TArray<FVector> BranchCoords();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector Calcqt(FVector natFreq, FVector dampRat, float deltaTime, float m);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector CalcL_t(FVector natFreq, FVector q_t);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector ConvLoadsToGlobal();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void FixPosition();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector GetCurrentOffset();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float GlobalToLocalOffset(FVector globOffset, int32 dir);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void TickReset();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void AddForce(FVector f);

private:

	bool bCrdsUpdThisTick;
	TArray<FVector> branchCoords;

	bool isOrigPosSet;
	FVector origRelPosition;

	bool velUpdThisTick;

	bool ltUpdThisTick;
	bool qtUpdThisTick;

	bool needClearForce;

	FVector un_;
};
