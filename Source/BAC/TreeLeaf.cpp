// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeLeaf.h"
#include "TreeNode.h"

// Sets default values
ATreeLeaf::ATreeLeaf()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	auto transformComp = CreateDefaultSubobject<USceneComponent>(FName("USceneComponent"), true);
	AddComponent(FName("USceneComponent"), true, FTransform(FVector()), transformComp);
	SetRootComponent(transformComp);
}

// Called when the game starts or when spawned
void ATreeLeaf::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreeLeaf::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	UpdateVerticesPosition();
}

void ATreeLeaf::DrawDebug()
{
	DrawDebugLine(GetWorld(), LeafVertices[0], LeafVertices[1], FColor::Cyan);
	DrawDebugLine(GetWorld(), LeafVertices[1], LeafVertices[2], FColor::Cyan);
	DrawDebugLine(GetWorld(), LeafVertices[2], LeafVertices[3], FColor::Cyan);
	DrawDebugLine(GetWorld(), LeafVertices[3], LeafVertices[0], FColor::Cyan);
}

void ATreeLeaf::SetVertices(TArray<FVector> verts)
{
	LeafVertices = verts;
	FVector leafCenter = FVector::ZeroVector;
	for (FVector v : LeafVertices)
	{
		leafCenter += v;
	}
	leafCenter /= verts.Num();
	SetActorLocation(leafCenter);

	LeafVertsRelativeOffset.InsertZeroed(0, LeafVertices.Num());
	for (int i = 0; i < LeafVertices.Num();i++)
	{
		LeafVertsRelativeOffset[i] = LeafVertices[i] - GetActorLocation();
	}
}

void ATreeLeaf::UpdateVerticesPosition()
{
	for (int i = 0; i < LeafVertices.Num(); i++)
	{
		LeafVertices[i] = GetActorLocation() + LeafVertsRelativeOffset[i];
	}
}
