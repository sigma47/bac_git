// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeSkeleton.h"
#include "TreeBone.h"
#include "TreeNode.h"
#include "TreeLeaf.h"
#include "TreeBranch.h"
#include "BACBlueprints.h"

// Sets default values
ATreeSkeleton::ATreeSkeleton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DrawDebugInfo = true;
	DrawBones = true;
	DrawNodes = true;
	DrawLeaves = true;

	finishedSkeletonSetup = false;

	auto transformComp = CreateDefaultSubobject<USceneComponent>(FName("USceneComponent"), true);
	AddComponent(FName("USceneComponent"), true, FTransform(FVector()), transformComp);
	SetRootComponent(transformComp);

	// constants
	c0 = 1.0;
	EImax = 5.0;

	// set currently as exportet mesh file doesnt include info
	Scale0 = 1300.0;
	Scale0Trunk = 1.0;
	Ratio = 0.01F;
	RatioPower = 1.2;
	LengthN.Add(1.0);
	LengthN.Add(0.3);
	LengthN.Add(0.6);
	LengthN.Add(0.4);
	for (int32 i = 0; i < 96; i++)
		LengthN.Add(0.1337);

	BaseSize = 0.4;
	Taper = 1.0;

	NaturalFrequency = FVector(0.75, 1.0, 1.1);
	DampeningRatio = FVector(0.35, 0.4, 0.45);

	// default density for Populus tremuloides (Quaking Aspen) according to http://db.worldagroforestry.org/wd/species/ at 05.04.14
	TreeDensity = 0.3723;

	UpdateStep = 1.0 / 60.0;
	currUpdateStep = 0;
}

// Called when the game starts or when spawned
void ATreeSkeleton::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATreeSkeleton::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (finishedSkeletonSetup)
	{
		currUpdateStep += DeltaTime;
		if (currUpdateStep >= UpdateStep)
		{
			while (currUpdateStep > UpdateStep)
				currUpdateStep -= UpdateStep;
			UpdateHarmonicOscs(DeltaTime);
		}
	}

	if (!DrawDebugInfo)	return;
	for (int i = 0; i < Bones.Num(); i++)
	{
		if (DrawBones)
			DrawDebugLine(GetWorld(), Bones[i]->StartNode->GetActorLocation(), Bones[i]->EndNode->GetActorLocation(), FColor(i % 255, (i - 255) % 255, 1.0, 1.0));
		if (DrawNodes)
		{
			DrawDebugSphere(GetWorld(), Bones[i]->StartNode->GetActorLocation(), FMath::Max(0.5F, Scale / 50), 4, FColor::Green);
			DrawDebugSphere(GetWorld(), Bones[i]->EndNode->GetActorLocation(), FMath::Max(0.5F, Scale / 50), 4, FColor::Blue);
			for (ATreeLeaf* leaf : Bones[i]->Leaves)
			{
				DrawDebugSphere(GetWorld(), leaf->GetActorLocation(), FMath::Max(0.5F, Scale / 50), 4, FColor::Red);
				DrawDebugSphere(GetWorld(), leaf->BranchNode->GetActorLocation(), FMath::Max(0.5F, Scale / 50), 4, FColor::Yellow);
			}
		}
		if (DrawLeaves)
		{
			for (ATreeLeaf* l : LostLeaves)
				l->DrawDebug();
			for (ATreeLeaf* leaf : Bones[i]->Leaves)
				leaf->DrawDebug();
		}
	}
	if (DrawNodes)
	{
		for (ATreeLeaf* tl : LostLeaves)
		{
			DrawDebugSphere(GetWorld(), tl->GetActorLocation(), FMath::Max(0.5F, Scale / 50), 4, FColor::Yellow);
		}
	}
}

bool ATreeSkeleton::IsOnLine(FVector p, FVector a, FVector b)
{
	float result = UBACBlueprints::PointToVecDst(a, b, p);

	if (result > IS_ON_BRANCH * Scale && result <= IS_OFF_BRANCH * Scale)
	{
		FString err = "dist is: ";
		err += FString::SanitizeFloat(result);
		UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);
	}

	return result < IS_ON_BRANCH * Scale;
}

void ATreeSkeleton::OrderBones()
{
	ATreeNode* rootNode = nullptr;

	//find lowest node
	for (ATreeBone* b : Bones)
	{
		ATreeNode* lowerNode = nullptr;
		if (b->StartNode->GetActorLocation().Z < b->EndNode->GetActorLocation().Z)
			lowerNode = b->StartNode;
		else
			lowerNode = b->EndNode;
		if (!rootNode)
			rootNode = lowerNode;
		else if (rootNode->GetActorLocation().Z > lowerNode->GetActorLocation().Z)
			rootNode = lowerNode;
	}

	TArray<ATreeNode*> nodeStack;
	ATreeBone* currAdjBone = nullptr;
	ATreeBranch* trunk = (ATreeBranch*)GetWorld()->SpawnActor<ATreeBranch>(ATreeBranch::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	trunk->SetActorLocation(rootNode->GetActorLocation());
	trunk->AttachRootComponentToActor(this);
	trunk->Generation = 0;
	trunk->ParentBranch = nullptr;
	trunk->offsetToParent = 0;

	if (rootNode->Bones.Num() > 1)
		UE_LOG(TreeAnimator, Log, L"Root should not have more than one bone.");

	rootNode->Bones[0]->AttachToBranch(trunk, rootNode);
	rootNode->AttachRootComponentToActor(rootNode->Bones[0], NAME_None, EAttachLocation::Type::KeepWorldPosition);

	nodeStack.AddUnique(rootNode);
	int32 i = 0;
	TArray<ATreeBone*> ignoreBones;
	TArray<ATreeBone*> reqParentBones;
	TArray<ATreeBone*> finishedBones;
	GetFloatingBranches(reqParentBones);

	while (i < nodeStack.Num())
	{
		for (ATreeBone* b : nodeStack[i]->Bones)
		{
			if (!b->ParentBranch)
			{
				ignoreBones.Add(b);
				currAdjBone = nodeStack[i]->GetAdjacentBoneWithParent(ignoreBones);

				if (currAdjBone->IsSameBranch(b))
				{
					b->AttachToBranch(currAdjBone->ParentBranch, nodeStack[i]);
				}
				else
				{
					ATreeBranch* newBranch = (ATreeBranch*)GetWorld()->SpawnActor<ATreeBranch>(ATreeBranch::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
					newBranch->SetActorLocation(rootNode->GetActorLocation());
					newBranch->AttachRootComponentToActor(this);
					newBranch->ParentBranch = currAdjBone->ParentBranch;
					newBranch->Generation = currAdjBone->ParentBranch->Generation + 1;
					if (newBranch->Generation >= 3)
					{
						UE_LOG(LogActor, Log, L"should not be a new branch (generation to high).");
					}
					b->AttachToBranch(newBranch, nodeStack[i]);
					newBranch->offsetToParent = FVector::Dist(newBranch->ParentBranch->GetRootNode()->GetActorLocation(), newBranch->GetRootNode()->GetActorLocation()) / Scale * 100;
				}	
				ignoreBones.Empty();
			}
			nodeStack.AddUnique(b->StartNode);
			nodeStack.AddUnique(b->EndNode);
			/*for (ATreeLeaf* l : b->Leaves)
			{
				nodeStack.AddUnique(l->BranchNode);
				l->BranchNode->Bones.Add(b);
			}*/

			finishedBones.AddUnique(b);
		}

		
		if (i == nodeStack.Num() - 1)
		{
			for (ATreeBone* freeBone : reqParentBones)
			{
				if (freeBone->ParentBranch)
					continue;
				for (ATreeBone* finBone : finishedBones)
				{
					if (finBone == freeBone)	
						continue;
					bool isOnLine = false;
					ATreeNode* anchorNode = nullptr;
					if (freeBone->StartNode->Bones.Num() == 1)
					{
						if (IsOnLine(freeBone->StartNode->GetActorLocation(), finBone->StartNode->GetActorLocation(), finBone->EndNode->GetActorLocation()))
						{
							isOnLine = true;
							anchorNode = freeBone->StartNode;
						}

					}
					else if (freeBone->EndNode->Bones.Num() == 1)
					{
						if (IsOnLine(freeBone->EndNode->GetActorLocation(), finBone->StartNode->GetActorLocation(), finBone->EndNode->GetActorLocation()))
						{
							isOnLine = true;
							anchorNode = freeBone->EndNode;
						}
					}

					if (isOnLine)
					{
						ATreeBranch* newBranch = (ATreeBranch*)GetWorld()->SpawnActor<ATreeBranch>(ATreeBranch::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
						newBranch->SetActorLocation(anchorNode->GetActorLocation());
						newBranch->AttachRootComponentToActor(this);
						newBranch->ParentBranch = finBone->ParentBranch;
						newBranch->Generation = finBone->ParentBranch->Generation + 1;
						if (newBranch->Generation >= 3)
						{
							UE_LOG(LogActor, Log, L"should not be a new branch (generation to high).");
						}
						//newBranch->RootRadius = CalcBranchRootRadius(
						//	FVector::Dist(anchorNode->GetActorLocation(), finBone->ParentBranch->GetRootNode()->GetActorLocation()) / finBone->ParentBranch->GetLength(),
						//	finBone->ParentBranch->Generation,
						//	finBone->ParentBranch);
						freeBone->AttachToBranch(newBranch, anchorNode);
						finishedBones.Add(freeBone);
						freeBone->AttachRootComponentToActor(finBone, NAME_None, EAttachLocation::Type::KeepWorldPosition);
						anchorNode->AttachRootComponentToActor(freeBone, NAME_None, EAttachLocation::Type::KeepWorldPosition);
						newBranch->offsetToParent = FVector::Dist(newBranch->ParentBranch->GetRootNode()->GetActorLocation(), newBranch->GetRootNode()->GetActorLocation()) / Scale * 100;
						//anchorNode->Radius = newBranch->RootRadius;

						nodeStack.AddUnique(freeBone->StartNode);
						nodeStack.AddUnique(freeBone->EndNode);

						break;
					}
				}
			}
		}

		if (nodeStack[i]->Radius < 0)
		{
			float lengthOffset = 0;
			float lengthMax = 0;
			if (nodeStack[i] == nodeStack[i]->GetParentBranch()->GetRootNode() && nodeStack[i]->GetParentBranch()->Generation != 0)
			{
				ATreeBone* parentBone = Cast<ATreeBone>(nodeStack[i]->Bones[0]->K2_GetRootComponent()->AttachParent->GetOwner());
				if (!parentBone)
				{
					UE_LOG(LogActor, Log, L"should be parented.");
				}
				else
				{
					lengthOffset = 0;
					lengthMax = GetLengthGenN(nodeStack[i]->GetParentBranch()->Generation, nodeStack[i]->GetParentBranch());
				}
			}
			else
			{
				lengthOffset = nodeStack[i]->GetRelativeBranchLength(Scale);
				lengthMax = GetLengthGenN(nodeStack[i]->GetParentBranch()->Generation, nodeStack[i]->GetParentBranch());

				/*FString s = "lengthOffset: ";
				s += FString::SanitizeFloat(lengthOffset);
				s += " lengthMax: ";
				s += FString::SanitizeFloat(lengthMax);
				UE_LOG(LogActor, Warning, TEXT("%s"), *s);*/
			}
			nodeStack[i]->Radius = CalcNodeRadius(nodeStack[i], lengthOffset, lengthMax);
		}

		i++;
		if (i >= 100000)
		{
			UE_LOG(TreeAnimator, Log, L"Could not escape order bones loop.");
			break;
		}
	}

	OrderLeafNodes();

	for (ATreeBone* b : Bones)
	{
		if (!b->ParentBranch)
			UE_LOG(TreeAnimator, Log, L"Failed to create branch for bone.");
		b->StartNode->FixPosition();
		b->EndNode->FixPosition();
	}

	finishedSkeletonSetup = true;
}

void ATreeSkeleton::OrderLeafNodes()
{
	for (ATreeBone* b : Bones)
	{
		float sizeDiff = b->Length();

		for (ATreeLeaf* l : b->Leaves)
		{
			float lengthOffset = FVector::Dist(l->BranchNode->GetActorLocation(), b->StartNode->GetActorLocation()) / Scale * 100 + b->StartNode->GetRelativeBranchLength(Scale);
			float lengthMax = GetLengthGenN(l->BranchNode->GetParentBranch()->Generation, l->BranchNode->GetParentBranch());

			l->BranchNode->Radius = CalcNodeRadius(l->BranchNode, lengthOffset, lengthMax);

			/*FString s = " is leaf wrong? lengthOffset: ";
			s += FString::SanitizeFloat(lengthOffset);
			s += " lengthMax: ";
			s += FString::SanitizeFloat(lengthMax);
			UE_LOG(LogActor, Warning, TEXT("%s"), *s);*/
		}
	}
}

float ATreeSkeleton::NewBranchRatio(ATreeNode* newRootNode, ATreeNode* rootStart, ATreeNode* rootEnd)
{
	return FVector::Dist(rootStart->GetActorLocation(), newRootNode->GetActorLocation()) / FVector::Dist(rootStart->GetActorLocation(), rootEnd->GetActorLocation());
}

float ATreeSkeleton::CalcNodeRadius(ATreeNode* node, float lengthOffset, float lengthMax)
{
	if (lengthOffset > lengthMax)
	{
		//UE_LOG(TreeAnimator, Log, L"Shouldnt really happen (reason calculation & value inaccuracy)."); 
		lengthOffset = lengthMax;
	}

	float radius = 0;

	ATreeBranch* parBranch = node->GetParentBranch();
	int32 generation = parBranch->Generation;
	if (generation == 0)
		radius = lengthMax * Ratio * Scale0Trunk;
	else
	{
		float parentLength = GetLengthGenN(parBranch->ParentBranch->Generation, parBranch->ParentBranch);
		radius = FMath::Pow(lengthMax / parentLength, RatioPower) * CalcNodeRadius(parBranch->ParentBranch->GetRootNode(), parBranch->offsetToParent, parentLength);
	}

	if (node != parBranch->GetRootNode())
	{
		float unit_taper = 0;
		if (Taper < 1 && Taper >= 0)
			unit_taper = Taper;
		else if (Taper < 2 && Taper >= 1)
			unit_taper = 2 - Taper;
		radius *= (1 - unit_taper * (lengthOffset / lengthMax));
	}
	node->Radius = radius;
	return radius;
}

float ATreeSkeleton::GetLengthGenN(int32 generation, ATreeBranch* parent)	//schould be expanded to handle different tree shapes - currently assumed 7
{
	if (generation < 0  || generation >= 10)
	{
		UE_LOG(TreeAnimator, Warning, L"Invalid generation");
		return -1000;
	}
	if (generation == 0)
		return LengthN[0] * Scale0;
	else if (generation == 1)
	{
		float lengthTrunk = GetLengthGenN(0, nullptr);

		/*FString err = "lengthTrunk: ";
		err += FString::SanitizeFloat(lengthTrunk);
		err += " parent->offsetToParent: ";
		err += FString::SanitizeFloat(parent->offsetToParent);
		err += " BaseSize * Scale0: ";
		err += FString::SanitizeFloat(BaseSize * Scale0);
		UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/

		float ratio = (lengthTrunk - parent->offsetToParent) / (lengthTrunk - BaseSize * Scale0);

		if (ratio <= 0.7)
			return lengthTrunk * LengthN[generation] * (0.5 + 0.5 * (ratio / 0.7));
		else
			return lengthTrunk * LengthN[generation] * (0.5 + 0.5 * (1.0 - ratio) / 0.3);
	}
	else
	{
		return LengthN[generation] * (GetLengthGenN(generation - 1, parent->ParentBranch) - 0.6 * parent->offsetToParent);
	}
}

void ATreeSkeleton::GetFloatingBranches(TArray<ATreeBone*>& floatBones)
{
	for (ATreeBone* b : Bones)
	{
		if ((b->StartNode->Bones.Num() == 1 || b->EndNode->Bones.Num() == 1) && !b->ParentBranch)
			floatBones.Add(b);
	}
}

float ATreeSkeleton::CalcGamma(float Lv_, int32 n)
{
	return 0.5 * PI * FMath::Sign(Lv_) * (1 - FMath::Exp(-c0 * Lv_)) / n;
}

float ATreeSkeleton::CalcPhi(float ki, FVector si, FVector Fi, float x, int32 iters)
{
	// gr��ere iters -> genaueres phi
	int32 i = 0;
	double tolerance = 0.00000001;

	// newton method of determining |Fi(t)|*|si|*sin(theta) - phi*ki = 0
	// for theta = acos(dot(Fi(t), si) / (|Fi(t)|*|si|)) - phi
	float FiLen = Fi.Size();
	float siLen = si.Size();
	float xOld = 0;
	for (int32 i = 0; i < iters; i++)
	{
		float acos = FMath::Acos(FVector::DotProduct(Fi, si) / (FiLen * siLen)) - x;
		acos = FMath::DegreesToRadians(acos);
		float fx = FiLen * siLen * FMath::RadiansToDegrees(FMath::Sin(acos)) - x*ki;
		float f_x = FiLen * siLen * FMath::RadiansToDegrees(FMath::Cos(acos)) * -1 - ki;
		if (FMath::Abs(f_x) <= tolerance * 0.00001)
			break;
		xOld = x;
		x = x - fx / f_x;
		if (FMath::Abs(x - xOld) / FMath::Abs(x) < tolerance)
			break;
	}

	/*FString err = "phi: ki: ";
	err += FString::SanitizeFloat(ki);
	err += " si: ";
	err += si.ToString();
	err += " Fi: ";
	err += Fi.ToString();
	err += " x: ";
	err += FString::SanitizeFloat(x);
	UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/

	return x;
}

void ATreeSkeleton::UpdateHarmonicOscs(float deltaTime)
{
	// update all branchCoordiantes and local loads of nodes in every segment
	for (ATreeBone* b : Bones)
	{
		b->StartNode->TickReset();
		b->EndNode->TickReset();
		b->TickReset();

		float beamLen = (b->EndNode->GetActorLocation() - b->StartNode->GetActorLocation()).Size();
		float beamVolume = ((PI * beamLen) / 3) * (b->StartNode->Radius * b->StartNode->Radius + b->StartNode->Radius * b->EndNode->Radius + b->EndNode->Radius * b->EndNode->Radius);
		float beamMass = TreeDensity * beamVolume;

		FVector startQt = b->StartNode->Calcqt(NaturalFrequency, DampeningRatio, deltaTime, beamMass);
		b->StartNode->CalcL_t(NaturalFrequency, startQt);

		if (b->EndNode == b->ParentBranch->GetEndNode())
		{
			FVector endQt = b->EndNode->Calcqt(NaturalFrequency, DampeningRatio, deltaTime, PI * b->EndNode->Radius*b->EndNode->Radius * 0.05 * Scale);
			b->EndNode->CalcL_t(NaturalFrequency, endQt);
		}

	}

	// for each segment PiPi+1, we first rotate it about the v axis
	// with an angle gamma, and then we rotate it about the vector Ni
	//	(Ni = Fi(t) � PiPi+1) with a bending angle phi i

	for (ATreeBone* b : Bones)
	{
		float lv = b->StartNode->L_t[1];
		lv = FMath::Sin(UGameplayStatics::GetRealTimeSeconds(GetWorld())) * 0.1;
		float i = b->ParentBranch->GetBoneIndex(b);
		float n = b->ParentBranch->GetBoneIndex(b->ParentBranch->GetEndNode()->Bones[0]);
		float gamma = CalcGamma(lv, n);

		AActor* parentActor = GetAttachParentActor();
		FRotator acRot = FRotator::ZeroRotator;
		if (parentActor)
			acRot = parentActor->GetActorRotation();
		FQuat vRot = FQuat(b->StartNode->BranchCoords()[1], gamma);
		acRot = (vRot * acRot.Quaternion()).Rotator();
		b->SetActorRotation(acRot);
		b->currGamma = gamma;

		FVector Fi = b->ParentBranch->CalcFi(i, n);
		Fi = FVector(1, 0, 0);
		Fi.X *= FMath::Sin(UGameplayStatics::GetRealTimeSeconds(GetWorld())) * 3;
		if (Fi != FVector::ZeroVector)
		{
			// EImax * (Dr - (Dr - Dt)*(i/(n-1)))^m
			float ki = EImax * FMath::Pow(b->StartNode->Radius * 2 - (b->StartNode->Radius * 2 - b->EndNode->Radius * 2) * (i / (n - 1)), 4);

			float phi = CalcPhi(ki, b->EndNode->GetActorLocation() - b->StartNode->GetActorLocation(), Fi, b->currPhi, 10);
			b->currPhi = phi;
			phi = FMath::DegreesToRadians(phi);

			FVector Ni = FVector::CrossProduct(Fi, b->EndNode->GetActorLocation() - b->StartNode->GetActorLocation());
			FQuat NiRot = FQuat(Ni, phi);
			acRot = (NiRot * acRot.Quaternion()).Rotator();
			b->SetActorRotation(acRot);
		}

	}
}