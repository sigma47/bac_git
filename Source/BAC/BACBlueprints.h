// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "BACBlueprints.generated.h"

/**
 * 
 */
UCLASS()
class BAC_API UBACBlueprints : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "FSBlueprintLibrary")
	static FString GetTreeAnimatorDir();

	UFUNCTION(BlueprintCallable, Category = "FSBlueprintLibrary")
	static float PointToVecDst(FVector start, FVector& end, FVector p);

};
