// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "WindGenerator.h"
#include "fftw3.h"


// Sets default values
AWindGenerator::AWindGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWindGenerator::BeginPlay()
{
	Super::BeginPlay();

	int ht = 2, wd = 2;
	fftw_complex *inp = fftw_alloc_complex(ht * wd);
	fftw_complex *fft = fftw_alloc_complex(ht * wd);
	fftw_complex *ifft = fftw_alloc_complex(ht * wd);

	fftw_plan plan_f = fftw_plan_dft_1d(wd *ht, inp, fft, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan plan_b = fftw_plan_dft_1d(wd * ht, fft, ifft, FFTW_BACKWARD, FFTW_ESTIMATE);

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j<2; j++)
		{
			inp[wd*i + j][0] = 1.0;
			inp[wd*i + j][1] = 0.0;
		}
	}

	//    fftw_execute(plan_f);

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j<2; j++)
		{
			fft[wd*i + j][1] = 0.0;
			if (i == 0 && j == 0)
				fft[wd*i + j][0] = 4.0;
			else
				fft[wd*i + j][0] = 0.0;
			
			FString s = "first: ";
			s += FString::SanitizeFloat(fft[wd*i + j][0]);
			s += " and: ";
			s += FString::SanitizeFloat(fft[wd*i + j][1]);
			UE_LOG(LogActor, Warning, TEXT("%s"), *s);
			//std::cout << fft[wd*i + j][0] << " and " << fft[wd*i + j][1] << std::endl;
		}
	}

	fftw_execute(plan_b);
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			FString s = "first: ";
			s += FString::SanitizeFloat(ifft[wd*i + j][0] / (double)(wd*ht));
			s += " and: ";
			s += FString::SanitizeFloat(ifft[wd*i + j][1] / (double)(wd*ht));
			UE_LOG(LogActor, Warning, TEXT("%s"), *s);
		}
			//std::cout << ifft[wd*i + j][0] / (double)(wd*ht) << " and " << ifft[wd*i + j][1] / (double)(wd*ht) << std::endl;
	}
}

// Called every frame
void AWindGenerator::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

