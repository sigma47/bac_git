// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeNode.h"
#include "TreeBone.h"
#include "TreeBranch.h"

// Sets default values
ATreeNode::ATreeNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	auto transformComp = CreateDefaultSubobject<USceneComponent>(FName("USceneComponent"), true);
	AddComponent(FName("USceneComponent"), true, FTransform(FVector()), transformComp);
	SetRootComponent(transformComp);

	Radius = -1.0;

	branchCoords.SetNumZeroed(3);
	L_t = FVector::ZeroVector;
	Lt = FVector::ZeroVector;
	Ft = FVector::ZeroVector;
	QT = FVector::ZeroVector;
	origRelPosition = FVector::ZeroVector;
	un_ = FVector::ZeroVector;
	isOrigPosSet = false;
	ltUpdThisTick = false;
	qtUpdThisTick = false;
	needClearForce = false;

}

// Called when the game starts or when spawned
void ATreeNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreeNode::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

ATreeBone* ATreeNode::GetAdjacentBoneWithParent(TArray<ATreeBone*> ignoreBranches = TArray<ATreeBone*>())
{
	bool isFine = true;
	for (ATreeBone* b : Bones)
	{
		if (b->ParentBranch)
		{
			for (ATreeBone* bIgn : ignoreBranches)
			{
				if (b == bIgn)
					isFine = false;
			}
			if (isFine)
				return b;
		}
		isFine = true;
	}
	UE_LOG(TreeAnimator, Log, L"Could not find adjacent bone with parent.");
	return nullptr;
}

int32 ATreeNode::GetLowestGeneration()
{
	int32 generation = 100;
	for (ATreeBone* b : Bones)
	{
		if (b->ParentBranch)
		{
			if (b->ParentBranch->Generation < generation)
				generation = b->ParentBranch->Generation;
		}
	}
	return generation;
}

ATreeBranch* ATreeNode::GetParentBranch()
{
	ATreeBranch* parBranch = nullptr;
	for (ATreeBone* b : Bones)
	{
		if (b->ParentBranch)
		{
			if (!parBranch)
			{
				if(b->ParentBranch->Generation >= 0)
					parBranch = b->ParentBranch;
			}
			else if (b->ParentBranch->Generation < parBranch->Generation && b->ParentBranch->Generation >= 0)
				parBranch = b->ParentBranch;
		}
	}
	return parBranch;
}

float ATreeNode::GetRelativeBranchLength(float scale)
{
	float dst = 0;
	ATreeBranch* parBranch = GetParentBranch();
	if (!parBranch)
	{
		UE_LOG(TreeAnimator, Log, L"Could not find any branch with this node.");
		return 0;
	}
	ATreeNode* branchRootNode = parBranch->GetRootNode();
	TArray<ATreeBone*> testedBones;

	ATreeNode* currNode = this;
	int32 maxiter = 0;
	while (currNode != branchRootNode)
	{
		for (ATreeBone* b : currNode->Bones)
		{
			if (b->EndNode == currNode)
			{
				dst += b->Length();
				currNode = b->StartNode;
				break;
			}
			testedBones.AddUnique(b);
		}
		if (maxiter >= 50)
		{
			UE_LOG(TreeAnimator, Log, L"Could not find path from this node to branch root node.");
			/*FString err = "this node: ";
			err += this->GetName();
			err += " branchroot node: ";
			err += branchRootNode->GetName();
			UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/

			FString err = "parentbranch: ";
			err += parBranch->GetName();
			err += " branchRootNode: ";
			err += branchRootNode->GetName();
			err += " targetNode: ";
			err += this->GetName();
			for (ATreeBone* bone : testedBones)
			{
				err += " testedBone: ";
				err += bone->GetName();
			}
			UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);
			break;
		}
		maxiter++;
	}
	//following two lines are to adjust sizing to unreal units
	dst /= scale;
	dst *= 100;
	return dst;
}

TArray<FVector> ATreeNode::BranchCoords()
{
	if (!bCrdsUpdThisTick)
	{
		ATreeNode* rootNode = GetParentBranch()->GetRootNode();
		FVector P0P1 = rootNode->Bones[0]->EndNode->GetActorLocation() - rootNode->GetActorLocation();
		FVector P0Pn = GetParentBranch()->GetEndNode()->GetActorLocation() - rootNode->GetActorLocation();

		branchCoords[1] = P0P1 / P0P1.Size();		
		FVector popnXbCoodsX = FVector::CrossProduct(P0Pn, branchCoords[1]);
		branchCoords[2] = popnXbCoodsX / popnXbCoodsX.Size();
		FVector bCoodsXXbCoodsY = FVector::CrossProduct(branchCoords[1], branchCoords[2]);
		branchCoords[0] = bCoodsXXbCoodsY / bCoodsXXbCoodsY.Size();
		bCrdsUpdThisTick = true;

		/*FString err = "branchcoords[0]: ";
		err += branchCoords[0].ToString();
		err += " branchcoords[1]: ";
		err += branchCoords[1].ToString();
		err += " branchcoords[2]: ";
		err += branchCoords[2].ToString();
		UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/
	}
	return branchCoords;
}

float DrivenHarmonicOscillator(float q, float u, float r, float natFreq, float dampRat, float m, float Lt)
{
	return -4 * PI * natFreq * dampRat * u - 4 * PI*PI * natFreq*natFreq * q + r * Lt / m;
}

float ExplicitEuler(float deltaTime, float iterations, float q0, float t0, float& un_, float r, float natFreq, float dampRat, float m, float Lt)
{
	float h = deltaTime / iterations;
	float qn = q0;
	float tn = t0;
	float un = un_;
	float qn1 = 0;
	float tn1 = 0;
	float un1 = 0;

	for (int32 i = 1; i < iterations; i++)
	{
		tn1 = tn + h;
		qn1 = qn + h * un;
		un1 = un + h * DrivenHarmonicOscillator(qn, un, r, natFreq, dampRat, m, Lt);

		tn = tn1;
		qn = qn1;
		un = un1;
	}

	un_ = un;
	return qn;
}

// dir: 0=u, 1=v, 2=w
float ATreeNode::GlobalToLocalOffset(FVector globOffset, int32 dir)
{
	/*FString err = "globaltolocaloffset. branchcoords[0][dir]: ";
	err += FString::SanitizeFloat(BranchCoords()[0][dir]);
	err += " globOffset[0]: ";
	err += FString::SanitizeFloat(globOffset[0]); 
	err += " branchcoords[1][dir]: ";
	err += FString::SanitizeFloat(BranchCoords()[1][dir]);
	err += " globOffset[1]: ";
	err += FString::SanitizeFloat(globOffset[1]);
	err += " branchcoords[2][dir]: ";
	err += FString::SanitizeFloat(BranchCoords()[2][dir]);
	err += " globOffset[2]: ";
	err += FString::SanitizeFloat(globOffset[2]);
	UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/
	return FVector(BranchCoords()[0][dir] * globOffset[0], BranchCoords()[1][dir] * globOffset[1], BranchCoords()[2][dir] * globOffset[2]).Size();
}

FVector ATreeNode::Calcqt(FVector natFreq, FVector dampRat, float deltaTime, float m)
{
	if (!qtUpdThisTick)
	{
		ATreeNode* rootNode = GetParentBranch()->GetRootNode();
		FVector P0Pn = GetParentBranch()->GetEndNode()->GetActorLocation() - rootNode->GetActorLocation();

		QT[0] = ExplicitEuler(deltaTime, 10, QT[0], 0.0, un_[0], 1, natFreq[0], dampRat[0], m, Lt[0]);
		QT[1] = ExplicitEuler(deltaTime, 10, QT[1], 0.0, un_[1], FVector::DotProduct(BranchCoords()[0], P0Pn), natFreq[1], dampRat[1], m, Lt[1]);
		QT[2] = ExplicitEuler(deltaTime, 10, QT[2], 0.0, un_[2], 1, natFreq[2], dampRat[2], m, Lt[2]);
		qtUpdThisTick = true;
		if (needClearForce)
			Lt = FVector::ZeroVector;
	}

	return QT;
}

FVector ATreeNode::CalcL_t(FVector natFreq, FVector q_t)
{
	if (!ltUpdThisTick)
	{
		FVector l_t = FVector::ZeroVector;
		for (int32 i = 0; i < 3; i++)
			l_t[i] = 4 * PI*PI * natFreq[i] * natFreq[i] * q_t[i];
		L_t = l_t;
		ltUpdThisTick = true;
	}
	return L_t;
}

//				( Lw(t) )
// (u, v, w) *  (	0	)
//				( Lu(t)	)
FVector ATreeNode::ConvLoadsToGlobal()
{
	return FVector(BranchCoords()[0][0] * L_t[2] + 0 + BranchCoords()[2][0] * L_t[0],
		BranchCoords()[0][1] * L_t[2] + 0 + BranchCoords()[2][1] * L_t[0],
		BranchCoords()[0][2] * L_t[2] + 0 + BranchCoords()[2][0] * L_t[0]);
}

FVector ATreeNode::GetCurrentOffset()
{
	/*FString err = "getcurrentoffset. parentact: ";
	err += GetAttachParentActor()->GetName();
	err += " diff: ";
	err += (GetActorLocation() - GetAttachParentActor()->GetActorLocation()).ToString();
	UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/
	return GetActorLocation() - GetAttachParentActor()->GetActorLocation();
}


void ATreeNode::FixPosition()
{
	if (!isOrigPosSet)
	{
		origRelPosition = GetActorLocation() - GetAttachParentActor()->GetActorLocation();
		isOrigPosSet = true;
	}
}

void ATreeNode::TickReset()
{
	bCrdsUpdThisTick = false;
	velUpdThisTick = false;
	ltUpdThisTick = false;
	qtUpdThisTick = false;
}

void ATreeNode::AddForce(FVector f)
{
	for (int32 i = 0; i < 3; i++)
		Lt[i] = BranchCoords()[0][i] * f[0] + BranchCoords()[1][i] * f[1] + BranchCoords()[2][i] * f[2];
	needClearForce = true;
}
