// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeFull.h"


// Sets default values
ATreeFull::ATreeFull()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATreeFull::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreeFull::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

