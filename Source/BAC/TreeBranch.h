// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TreeBranch.generated.h"

class ATreeBone;
class ATreeNode;

UCLASS()
class BAC_API ATreeBranch : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ATreeBone*> Bones;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Generation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATreeBranch* ParentBranch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float offsetToParent;

	// Sets default values for this actor's properties
	ATreeBranch();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float GetLength();

	// returns the childed bones parent - this is not the root bone of the branch. just the previous bone in the branch
	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeBone* GetRootingBone(ATreeBone* childedBone);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeNode* GetRootNode();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeNode* GetEndNode();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	int32 GetBoneIndex(ATreeBone* bone);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	ATreeBone* GetBoneAtIndex(int32 idx);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector CalcFi(int32 i, int32 n);

};
