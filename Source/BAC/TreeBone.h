// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TreeBone.generated.h"

class ATreeNode;
class ATreeLeaf;
class ATreeBranch;

UCLASS()
class BAC_API ATreeBone : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATreeNode* StartNode;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATreeNode* EndNode;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATreeBranch* ParentBranch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ATreeLeaf*> Leaves;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector> NodeOffsets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float currPhi;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float currGamma;

	// Sets default values for this actor's properties
	ATreeBone();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float Length();
	
	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	float LengthSqr();
	
	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	FVector Direction();
	
	//UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	//FVector OrigDirection();

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	bool IsSameBranch(ATreeBone* b);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void AttachToBranch(ATreeBranch* parent, ATreeNode* start);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void MoveBoneWOChildren(FVector newPosition);

	UFUNCTION(BlueprintCallable, Category = "TreeAnimator")
	void TickReset();

private:

	bool lenCUpdThisTick;
	float lengthCache;

	bool dirCUpdThisTick;
	FVector dirCache;
};
