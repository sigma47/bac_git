// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "TreeBranch.h"
#include "TreeBone.h"
#include "TreeNode.h"

// Sets default values
ATreeBranch::ATreeBranch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	auto transformComp = CreateDefaultSubobject<USceneComponent>(FName("USceneComponent"), true);
	AddComponent(FName("USceneComponent"), true, FTransform(FVector()), transformComp);
	SetRootComponent(transformComp);

	Generation = -1;
	offsetToParent = -1;
}

// Called when the game starts or when spawned
void ATreeBranch::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreeBranch::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

float ATreeBranch::GetLength()
{
	float length = 0;
	for (ATreeBone* b : Bones)
	{
		length += b->Length();
	}
	return length;
}

ATreeBone* ATreeBranch::GetRootingBone(ATreeBone* childedBone)
{
	for(ATreeBone* b : Bones)
	{
		if (b->EndNode == childedBone->StartNode)
			return b;
	}
	//UE_LOG(TreeAnimator, Log, L"Could not find rooting bone or is root bone of branch.");
	return nullptr;
}

ATreeNode* ATreeBranch::GetRootNode()
{
	return Bones[0]->StartNode;
	/*for (ATreeBone* b : Bones)
	{
		if (b->StartNode->GetActorLocation() == GetActorLocation())
			return b->StartNode;
	}
	UE_LOG(TreeAnimator, Log, L"Could not find root node.");
	return nullptr;*/
}

ATreeNode* ATreeBranch::GetEndNode()
{
	/*FString err = "endnode of branch: ";
	err += GetName();
	err += " is: ";
	err += Bones[Bones.Num()-1]->EndNode->GetName();
	UE_LOG(TreeAnimator, Log, TEXT("%s"), *err);*/
	return Bones[Bones.Num()-1]->EndNode;
}

int32 ATreeBranch::GetBoneIndex(ATreeBone* bone)
{
	for (int32 i = 0; i < Bones.Num(); i++)
	{
		if (Bones[i] == bone)
			return i;
	}
	return 0;
	/*ATreeNode* startNode = GetRootNode();
	int32 idx = 0;
	while (startNode != bone->StartNode)
	{
		for (ATreeBone* b : startNode->Bones)
		{
			if (b->StartNode == startNode)
			{
				startNode = b->EndNode;
				break;
			}
		}

		idx++;
		if (idx >= 50)
		{
			UE_LOG(TreeAnimator, Log, L"Could not find bone index.");
			break;
		}
	}
	return idx;*/
}

ATreeBone* ATreeBranch::GetBoneAtIndex(int32 idx)
{
	return Bones[idx];
	/*ATreeNode* startNode = GetRootNode();
	int32 i = 0;
	do
	{
		for (ATreeBone* b : Bones)
		{
			if (b->StartNode == startNode)
			{
				startNode = b->EndNode;
				if (i == idx)
					return b;
				else
					break;
			}
		}

		idx++;
		if (idx >= 50)
		{
			UE_LOG(TreeAnimator, Log, L"Could not find bone at index.");
			break;
		}
	} while (i != idx);
	return nullptr;*/
}

FVector ATreeBranch::CalcFi(int32 i, int32 n)
{
	FVector fi = FVector::ZeroVector;

	for (ATreeBone* b : Bones)
	{
		if(GetBoneIndex(b) >= i)
		{
			fi += b->StartNode->ConvLoadsToGlobal();
			if (b->EndNode == GetEndNode())
				fi += b->EndNode->ConvLoadsToGlobal();
		}
	}

	fi /= n;
	return fi;
}