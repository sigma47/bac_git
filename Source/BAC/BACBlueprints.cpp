// Fill out your copyright notice in the Description page of Project Settings.

#include "BAC.h"
#include "BACBlueprints.h"

FString UBACBlueprints::GetTreeAnimatorDir()
{
	FString fileLoc = FPaths::GameContentDir();
	fileLoc += "/";
	return fileLoc;
}

float UBACBlueprints::PointToVecDst(FVector start, FVector& end, FVector p) {
	// Return minimum distance between line segment vw and point p
	const float l2 = FVector::DistSquared(start, end);  // i.e. |w-v|^2 -  avoid a sqrt
	if (l2 == 0.0) return FVector::Dist(p, start);   // v == w case
	// Consider the line extending the segment, parameterized as v + t (w - v).
	// We find projection of point p onto the line. 
	// It falls where t = [(p-v) . (w-v)] / |w-v|^2
	const float t = FVector::DotProduct(p - start, end - start) / l2;
	if (t < 0.0) return FVector::Dist(p, start);       // Beyond the 'v' end of the segment
	else if (t > 1.0) return FVector::Dist(p, end);  // Beyond the 'w' end of the segment
	const FVector projection = start + t * (end - start);  // Projection falls on the segment
	end = projection;
	return FVector::Dist(p, projection);
}

